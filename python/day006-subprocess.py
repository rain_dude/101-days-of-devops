#!/usr/bin/env python3

import subprocess

def run():
    return run_textual()

def run_textual():
    command = "ls -l"
    executor = subprocess.Popen(command, shell = True, stdout = subprocess.PIPE, stderr = subprocess.PIPE, universal_newlines = True)
    executor.wait()
    out, err = executor.communicate()
    return out

def run_binarry():
    command = "ls -l"
    executor = subprocess.Popen(command, shell = True, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    executor.wait()
    out, err = executor.communicate()
    return out

def main():
    result = run()
    print(result)

# Keep formatting __name__=="__main__"! Adding spaces breaks execution from Emacs... Why? Don't know.
if __name__=="__main__":
    main()
