#!/usr/bin/env python3

import paramiko

def run(connection_params: dict[str, str]):
    ssh = paramiko.SSHClient()
    ssh.connect(hostname = connection_params["hostname"], username = connection_params["username"], password = connection_params["password"])
    stdin, stdout, stderr = ssh.exec_command('free -m')
    print(stdout.readlines())
    ssh.close()

def main():
    connection_params: dict[str, str] = {"hostname": '<ip address of remote machine', "username": 'centos', "password": '<password of remote server>'}
    result = run(connection_params)
    print(f"Result: {result}")

# Keep formatting __name__=="__main__"! Adding spaces breaks execution from Emacs... Why? Don't know.
if __name__=="__main__":
    main()
