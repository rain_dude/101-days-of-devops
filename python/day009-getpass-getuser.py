#!/usr/bin/env python3

import getpass

def run():
    guessed = getpass.getpass(prompt = "Guess user: ")
    user = getpass.getuser()
    if user == guessed:
        return "User: " + user

    return "Today, nothing"

def main():
    result = run()
    print(result)

# Keep formatting __name__=="__main__"! Adding spaces breaks execution from Emacs... Why? Don't know.
if __name__=="__main__":
    main()
