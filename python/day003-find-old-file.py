#!/usr/bin/env python3

from pathlib import Path
from datetime import datetime, timedelta

def run(days_old):
    path_to_scan = Path.home()
    filesystem_entries = path_to_scan.glob('*')
    files = filter(lambda entry: entry.is_file, filesystem_entries)

    return filter(lambda file: is_old_enough(file, days_old), files)

def is_old_enough(file, days_old):
    today = datetime.today()
    target_day = today - timedelta(days = days_old)
    file_mtime = datetime.fromtimestamp(file.stat().st_mtime)

    return file_mtime < target_day

def main():
    result = run(7)
    print(list(result))

# Keep formatting __name__=="__main__"! Adding spaces breaks execution from Emacs... Why? Don't know.
if __name__=="__main__":
    main()
