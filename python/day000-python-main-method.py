#!/usr/bin/env python3

def run():
    return ""

def main():
    result = run()
    print(result)

# Keep formatting __name__=="__main__"! Adding spaces breaks execution from Emacs... Why? Don't know.
if __name__=="__main__":
    main()
