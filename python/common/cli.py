import os
import argparse
import sys
from typing import Dict

def get_all_options(options: Dict[str, str]):
    parser = argparse.ArgumentParser()
    for option, opt_help in options.items():
        long_option = f"--{option}"
        parser.add_argument(long_option, type = str, help = opt_help)

    args = parser.parse_args()

    validate_options(parser, args, *options.keys())

    underscored_result: Dict[str, str] = vars(args)
    final_result = dict(map(lambda key, val: (key.replace('_', '-'), val), underscored_result.keys(), underscored_result.values()))

    return final_result

def validate_options(parser: argparse.ArgumentParser, args: argparse.Namespace, *all_expected_options: str):
    for expected_option in all_expected_options:
        underscored_option = expected_option.replace('-', '_')
        if not args.__dict__.get(underscored_option):
            parser.error(f"Missing {expected_option} argument")

def validate_path(dir: str):
    valid = is_path_valid(dir)
    if not valid:
        sys.exit(f"Invalid dir: {dir}")

def is_path_valid(dir: str):
    return os.path.exists(dir)
