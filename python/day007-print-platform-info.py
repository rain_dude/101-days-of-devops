#!/usr/bin/env python3

import platform
import os

def run():
    version = platform.python_version()
    parts = version.split('.')
    major = parts[0]
    if int(major) < 3:
        return f"Unsupported major: {major}"

    minor = parts[1]
    if int(minor) < 10:
        return f"Unsupported minor: {minor}"

    return f"Supported: {version}"

def main():
    result = run()
    print(result)

# Keep formatting __name__=="__main__"! Adding spaces breaks execution from Emacs... Why? Don't know.
if __name__=="__main__":
    main()
