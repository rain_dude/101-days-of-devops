#!/usr/bin/env python3

from itertools import chain
from pathlib import Path
from common.cli import get_all_options, validate_path

def run() -> Path:
    dir: str = get_input_dir()
    property_paths: list[str] = get_property_paths_from(dir)
    unsorted_property_key_groups: list[list[str]] = list(map(lambda path: get_property_keys_from(path), property_paths))
    unsorted_propery_keys: set[str] = set(chain.from_iterable(unsorted_property_key_groups))
    property_keys: list[str] = sorted(unsorted_propery_keys)

    result_path = Path(dir).joinpath("keys.properties")
    with open(result_path, "w") as file:
        file.writelines(f"{key}=\n" for key in property_keys)

    return result_path

def get_input_dir():
    options = get_all_options({"dir": "Directory"})

    dir = options["dir"]

    validate_path(dir)

    return dir

def get_property_paths_from(dir: str) -> list[str]:
    filesystem_entries = Path(dir).glob('*.properties')
    file_paths: list[str] = list(map(lambda path: Path(path).as_posix(), filesystem_entries))

    return file_paths

def get_property_keys_from(path: str) -> list[str]:
    with open(path, "r") as file:
        properties: list[str] = list(filter(lambda line: not line.startswith("#") and line.strip() != "", file))
        keys = list(map(lambda line: line.split("=")[0], properties))

        return keys

def main():
    result = run()
    print(f"Result:\n{result}")

# Keep formatting __name__=="__main__"! Adding spaces breaks execution from Emacs... Why? Don't know.
if __name__=="__main__":
    main()
