#!/usr/bin/env python3

from common.cli import get_all_options
from itertools import chain
import re

# Example Apache log line:
# 192.168.0.1 — — [23/Apr/2017:05:54:36 -0400] “GET / HTTP/1.1” 403 3985 “-” “Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36”
def run() -> dict[str, int]:
    options = get_all_options({"log-file": "Apache log file path"})
    path = options["log-file"]

    ips: list[str] = extract_ips_from(path)

    frequency_by_ips = calculate_frequency_in(ips)
    return frequency_by_ips

def extract_ips_from(path: str) -> list[str]:
    with open(path, "r") as file:
        ip_groups: list[list[str]] = list(map(lambda line: extract_ips(line), file))
        ips: list[str] = list(chain.from_iterable(ip_groups))
        return ips

def extract_ips(line: str) -> list[str]:
    ip_re = re.compile(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')
    ips = ip_re.findall(line)
    return ips

def calculate_frequency_in(strings: list[str]):
    frequency_by_strings = {}
    for string in strings:
        if string in frequency_by_strings:
            current = frequency_by_strings[string]
            frequency_by_strings[string] = current + 1
        else:
            frequency_by_strings[string] = 1
    return frequency_by_strings

def main():
    result = run()
    print(f"Number of IP-s in log file:\n{result}")

# Keep formatting __name__=="__main__"! Adding spaces breaks execution from Emacs... Why? Don't know.
if __name__=="__main__":
    main()
