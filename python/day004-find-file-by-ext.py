#!/usr/bin/env python3

from pathlib import Path

def run(file_ext):
    path_to_scan = Path('~/.config/').expanduser()
    filesystem_entries = path_to_scan.glob('*')
    files = filter(lambda entry: entry.is_file, filesystem_entries)

    return filter(lambda file: has_file_ext(file, file_ext), files)

def has_file_ext(file, file_ext):
    return file.suffix == file_ext

def main():
    result = run(".conf")
    print(list(result))

# Keep formatting __name__=="__main__"! Adding spaces breaks execution from Emacs... Why? Don't know.
if __name__=="__main__":
    main()
