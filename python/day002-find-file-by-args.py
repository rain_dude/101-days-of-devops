#!/usr/bin/env python3

from common.cli import validate_options

import argparse
import os

def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir", type = str, help = "Directory")
    parser.add_argument("--filename", type = str, help = "Filename")
    args = parser.parse_args()

    validate_options(parser, args, "dir", "filename")

    return vars(args)


def search(file_search_args):
    path = os.path.join(file_search_args["dir"], file_search_args["filename"])

    return os.path.exists(path)

def main():
    file_search_args = parseArgs()
    found = search(file_search_args)
    print(f"Found? {found}")

# Keep formatting __name__=="__main__"! Adding spaces breaks execution from Emacs... Why? Don't know.
if __name__=="__main__":
    main()
