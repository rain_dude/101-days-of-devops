#!/usr/bin/env python3

from pathlib import Path
from typing import Iterable

from common.cli import get_all_options, validate_path

def run():
    input_dir = get_input_dir()
    files_in_dir = get_filenames_in(input_dir)
    formatted = format_files(files_in_dir)

    return formatted

def get_input_dir():
    options = get_all_options({"dir": "Directory"})

    dir = options["dir"]

    validate_path(dir)

    return dir

def get_filenames_in(dir: str):
    filesystem_entries = Path(dir).glob('*')
    filenames = map(lambda entry: entry.name, filesystem_entries)

    return filenames

def format_files(files: Iterable[str]):
    return " ".join(files)

def main():
    result = run()
    print(result)

# Keep formatting __name__=="__main__"! Adding spaces breaks execution from Emacs... Why? Don't know.
if __name__=="__main__":
    main()
