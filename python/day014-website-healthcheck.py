#!/usr/bin/env python3

import requests
import bs4
import random

def run(url: str):
    response = requests.get(url = url, timeout = 5)
    status_code = response.status_code
    if status_code >= 300 and status_code < 200:
        return f"Not 2XX: got = {status_code}"

    soup = bs4.BeautifulSoup(response.text, "html.parser")
    elems = soup.select('#post-67 > div > div > header > h2')
    if len(elems) == 0:
        return f"Missing \"Welcome!\""

    welcome = elems[0].text
    if welcome != "Welcome":
        return f"Missing \"Welcome!\": got = {welcome}"

    return f"Healthy"

def main():
    url_healthy = "http://100daysofdevops.com"
    url_unhealthy = "http://100daysofdevops.com/contact-us/"
    if random.choice([True, False]):
        result = run(url_healthy)
    else:
        result = run(url_unhealthy)
    print(f"Result: {result}")

# Keep formatting __name__=="__main__"! Adding spaces breaks execution from Emacs... Why? Don't know.
if __name__=="__main__":
    main()
